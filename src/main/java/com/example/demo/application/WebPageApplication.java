package com.example.demo.application;

import com.example.demo.dto.WebPage;
import com.example.demo.repository.WebPageRepository;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Transactional
public class WebPageApplication {
    public WebPageApplication(WebPageRepository webPageRepository) {
        this.webPageRepository = webPageRepository;
    }

    private final WebPageRepository webPageRepository;

    public Long create(WebPage webPage) {
        return webPageRepository.save(new com.example.demo.dao.WebPage(webPage)).getId();
    }

    public List<com.example.demo.dto.WebPage> findAll() {
        return webPageRepository.findAll()
                .stream()
                .map(com.example.demo.dao.WebPage::toDto)
                .collect(Collectors.toList());
    }

    public com.example.demo.dto.WebPage getById(Long id) {
        return webPageRepository.findById(id).get().toDto();
    }

    public void update(WebPage webPage) {
        webPageRepository.save(new com.example.demo.dao.WebPage(webPage));
    }

    public void deleteById(Long id) {
        webPageRepository.deleteById(id);
    }

}
