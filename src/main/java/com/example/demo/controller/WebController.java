package com.example.demo.controller;

import com.example.demo.application.WebPageApplication;
import com.example.demo.dto.WebPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/webpage")
public class WebController {
    @Autowired
    WebPageApplication webPageApplication;

    @GetMapping
    public String getList(Model model) {
        model.addAttribute("list", webPageApplication.findAll());
        return "webpageList";
    }

    @GetMapping("/create")
    public String getCreateForm() {
        return "createWebpage";
    }

    @GetMapping("/edit/{id}")
    public String getEditForm(@PathVariable Long id, Model model) {
        System.out.println(webPageApplication.getById(id));
        model.addAttribute("webpage", webPageApplication.getById(id));
        return "editWebpage";
    }

    @PostMapping
    public String create(@ModelAttribute WebPage webPage) {
         webPageApplication.create(webPage);
        return "redirect:/webpage";
    }

    @PostMapping("/update")
    public String update(@ModelAttribute WebPage webPage) {
        webPageApplication.update(webPage);
        return "redirect:/webpage/edit" + webPage.getId();
    }

}
