package com.example.demo.repository;

import com.example.demo.dao.WebPage;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.Optional;

public interface WebPageRepository extends Repository<WebPage, Long> {
    WebPage save(WebPage webPage);
    List<WebPage> findAll();
    Optional<WebPage> findById(Long id);
    void deleteById(Long id);

}
