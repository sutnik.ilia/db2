package com.example.demo.dto;


import java.util.List;

public class WebPage {
    private Long id;
    private String description;
    private List<Comments> comments;

    public List<Comments> getComments() {
        return comments;
    }

    public void setComments(List<Comments> comments) {
        this.comments = comments;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "WebPage{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", comments=" + comments +
                '}';
    }
}
