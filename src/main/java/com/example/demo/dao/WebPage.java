package com.example.demo.dao;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Entity
public class WebPage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String description;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Comments> comments;

    public WebPage() {
    }

    public WebPage(com.example.demo.dto.WebPage webPage) {
        this.id = webPage.getId();
        this.description = webPage.getDescription();
        this.comments = (List<Comments>) new Comments(webPage.getComments());
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Comments> getComments() {
        return comments;
    }

    public void setComments(List<Comments> comments) {
        this.comments = comments;
    }

    public com.example.demo.dto.WebPage toDto() {
        com.example.demo.dto.WebPage webPage = new com.example.demo.dto.WebPage();
        webPage.setId(this.getId());
        webPage.setDescription(this.getDescription());
        webPage.setComments(this.getComments().stream().flatMap(c::toDto).collect(Collectors.toList()));
        return webPage;
    }

    @Override
    public String toString() {
        return "WebPage{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", comments=" + comments +
                '}';
    }
}
