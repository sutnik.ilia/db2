package com.example.demo.dao;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Entity
@Table(name = "comment")
public class Comments {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String comment;

    public Comments() {

    }

    public Comments(List<com.example.demo.dto.Comments> comments) {
        this.comment = comments.get().getComment();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public List<com.example.demo.dto.Comments> toDto() {
        com.example.demo.dto.Comments comments = new com.example.demo.dto.Comments();
        comments.setComment(this.getComment());
        return Stream.of(comments).collect(Collectors.toList());
    }

    @Override
    public String toString() {
        return "Comments{" +
                "id=" + id +
                ", comment='" + comment + '\'' +
                '}';
    }
}
